<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>


<%@ page import="com.normz.connection.*,com.normz.controller.UpdateUserProcess,java.sql.ResultSet,java.sql.SQLException"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update User</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>
</head>
<body>
	<%
		String id = request.getParameter("id");
		UpdateUserProcess uup = new UpdateUserProcess();
		ResultSet rs = uup.viewInfo(id);
		try {
			while(rs.next()) {
	%>
	<form action="updateuser" method="post">
		<input type="hidden" value='<%=rs.getString("id")%>' name="id">
		<table>
			<tr>
				<td>First Name</td>
				<td>:</td>
				<td><input type="text" name="fname" value='<%=rs.getString("firstname")%>'></td>
			</tr>
			<tr>
				<td>Last Name</td>
				<td>:</td>
				<td><input type="text" name="lname" value='<%=rs.getString("lastname")%>'></td>
			</tr>
			<tr>
				<td>Address</td>
				<td>:</td>
				<td><input type="text" name="address" value='<%=rs.getString("address")%>'></td>
			</tr>
			<tr>
				<td>Date of Birth</td>
				<td>:</td>
				<td><input type="text" name="dob" id="datepicker" value='<%=rs.getString("DoB")%>'></td>
			</tr>
			<tr>
				<td>Username</td>
				<td>:</td>
				<td><input type="text" name="username" value='<%=rs.getString("username") %>'></td>
			</tr>
		</table>
	<button type="submit">Update</button>
	<% }
} catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} %>
	</form>
</body>
</html>