<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="javax.servlet.http.HttpSession"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration Form</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>
</head>
<body>
	<%
		HttpSession mySession = request.getSession(false);
		String name = (String) mySession.getAttribute("UserSession");
	if(name != null) {
	%>

	<form action="registeruser" method="post">
		<table>
			<tr>
				<Td>First Name</Td>
				<td>:</td>
				<td><input type="text" name="fname"></td>
			</tr>
			<tr>
				<td>Last Name</td>
				<td>:</td>
				<td><input type="text" name="lname"></td>
			</tr>
			<tr>
				<td>Address</td>
				<td>:</td>
				<td><input type="text" name="address"></td>
			</tr>
			<tr>
				<td>Username</td>
				<td>:</td>
				<td><input type="text" name="username"></td>
			</tr>
			<tr>
				<td>Password</td>
				<td>:</td>
				<td><input type="password" name="password"></td>
			</tr>
			<tr>
				<td>Confirm Password</td>
				<td>:</td>
				<td><input type="password" name="confirmPassword"></td>
			</tr>
			<tr>
				<td>Birthday</td>
				<td>:</td>
				<td><input type="text" name="dob" id="datepicker"></td>
			</tr>

		</table>
		<table>
			<tr>
				<td><button type="submit">Register</button>
					</form></td>
			</tr>
			<tr>
				<td><form action="manageusers">
						<button type="submit">Manage Users</button>
					</form></td>
			</tr>
		</table>
		
<% } 
	else {
		out.println("Please login first. <a href='login.jsp'>Login.</a>");
	}
	
	%>
</body>
</html>