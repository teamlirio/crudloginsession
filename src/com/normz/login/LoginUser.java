package com.normz.login;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/loginuser")
public class LoginUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String loginUsername = request.getParameter("loginUsername");
		String loginPassword = request.getParameter("loginPassword");
		LoginUserProcess lup = new LoginUserProcess();
		String result = lup.loginUser(loginUsername, loginPassword);
		if(loginUsername.equals(result)) {
			out.println("<h1>Hello, " + result + "! Welcome!</h1>");
			HttpSession session = request.getSession();
			session.setAttribute("UserSession", result);
			session.setMaxInactiveInterval(60);
			out.println("Click <a href='registration.jsp'>here</a> to your control panel.");
			
		}
		else {
			out.println(result);
		}
		
		out.close();
	}
	
}
