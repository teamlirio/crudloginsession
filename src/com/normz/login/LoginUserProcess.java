package com.normz.login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.normz.connection.CrudConnection;

public class LoginUserProcess {
	private CrudConnection cc = new CrudConnection();
	public String loginUser(String username, String password) {
		System.out.println(username + " " + password);
		if(username != null && password != null) {
			PreparedStatement pstate = null;
			try {
				String query = "Select * from login where sUsername = ? and sPassword = ?";
				pstate = cc.getConnection().prepareStatement(query);
				pstate.setString(1, username);
				pstate.setString(2, password);	
				ResultSet rs = pstate.executeQuery();
				while(rs.next()) {
					return rs.getString("sUsername");
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		return "Invalid username or password. <a href='login.jsp'>Login</a>";
		
		
	}
	
	
}
