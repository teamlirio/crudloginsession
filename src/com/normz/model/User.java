package com.normz.model;

import java.util.Date;

public class User {
	private String attempt;
	private String id;
	private String fname;
	private String lname;
	private String address;
	private String password;
	private String username;
	private Date date;
	private String confirmPassword;
	
	public User() {
		
	}
	public User(String attempt) {
		this.attempt = attempt;
	}
	
	public User(String id, String fname, String lname, String address, Date date, String username) {
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.address = address;
		this.date = date;
		this.username = username;
	}
	public User(String id, String fname, String lname, String address, String password, String username, Date date) {
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.address = address;
		this.password = password;
		this.username = username;
		this.date = date;
	}
	public User(String fname, String lname, String address, String password, String username, Date date, String confirmPassword) {
		this.fname = fname;
		this.lname = lname;
		this.address = address;
		this.password = password;
		this.username = username;
		this.date = date;
		this.confirmPassword = confirmPassword;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getAttempt() {
		return attempt;
	}

	public void setAttempt(String attempt) {
		this.attempt = attempt;
	}
	
	
}
