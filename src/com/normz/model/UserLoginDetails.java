package com.normz.model;

public class UserLoginDetails {
	private String username;
	private String password;
	private String attempt;
	
	
	public UserLoginDetails(String attempt) {
		this.attempt = attempt;
	}
	public UserLoginDetails() {
		
	}
	public UserLoginDetails(String username, String password) {
		this.username = username;
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAttempt() {
		return attempt;
	}
	public void setAttempt(String attempt) {
		this.attempt = attempt;
	}
	
}
