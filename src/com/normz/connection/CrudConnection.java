package com.normz.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CrudConnection {
	private static final String URL = Messages.getString("CrudConnection.URL"); //$NON-NLS-1$
	private static final String USER = Messages.getString("CrudConnection.USER"); //$NON-NLS-1$
	private static final String PASSWORD = Messages.getString("CrudConnection.PASSWORD"); //$NON-NLS-1$
	private static final String DRIVER = Messages.getString("CrudConnection.DRIVER"); //$NON-NLS-1$
	private Connection c;
	public Connection getConnection() {
		try {
			Class.forName(DRIVER);
			c = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return c;
	}
	
}
