package com.normz.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.normz.model.User;
@WebServlet("/manageusers")
public class ManageUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		try {
			ManageUsersProcess mup = new ManageUsersProcess();
			ResultSet rs = mup.manageUsers();
			
			out.println("<table>");
			out.println("<tr>");
			out.println("<td width='130px'>Firstname</td>");
			out.println("<td width='130px'>Lastname</td>");
			out.println("<td width='130px'>Username</td>");
			out.println("<td width='130px'>Address</td>");
			out.println("<td width='130px'>Date of Birth</td>");
			out.println("<td width='130px'>Actions</td>");
			out.println("</tr></table>");
			while(rs.next()) {
			out.println("<table>");
			out.println("<tr>");
			out.println("<td width='130px'>" + rs.getString("firstname") + "</td>");
			out.println("<td width='130px'>" + rs.getString("lastname") + "</td>");
			out.println("<td width='130px'>" + rs.getString("username") + "</td>");
			out.println("<td width='130px'>" + rs.getString("address") + "</td>");
			out.println("<td width='130px'>" + rs.getDate("DoB") + "</td>");
			out.println("<td width='130px'><a href='edituser.jsp?id=" + rs.getString("id") + "'>Edit</a> | <a href='deleteuser?id=" + rs.getString("id") + "'>Delete</a> </td>");
			
			out.println("</tr></table>");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		out.close();
	}

}
