package com.normz.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.normz.model.*;


@WebServlet("/updateuser")
public class UpdateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private User user;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		try {
			String id = request.getParameter("id");
			String fname = request.getParameter("fname");
			String lname = request.getParameter("lname");
			String address = request.getParameter("address");
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date dob = sdf.parse(request.getParameter("dob"));
			String username = request.getParameter("username");
			UpdateUserProcess uup = new UpdateUserProcess();
			user = uup.updateUser(new User(id, fname,lname,address,dob,username));
			out.println(user.getAttempt());
			out.println("<a href='registration.jsp'>Registration page</a>");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
