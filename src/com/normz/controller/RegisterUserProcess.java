package com.normz.controller;
import java.sql.PreparedStatement;
import java.sql.SQLException;


import com.normz.connection.CrudConnection;
import com.normz.model.User;
public class RegisterUserProcess {
	private CrudConnection cc = new CrudConnection();
	public User registerUser(User user){
		if(user.getPassword().equals(user.getConfirmPassword())) {
			
			try {
				
				String query = "INSERT INTO crud (firstname, lastname, address, DoB, username, password)"
						+ " values(?,?,?,?,?,?)";
				PreparedStatement pstate = cc.getConnection().prepareStatement(query);
				pstate.setString(1, user.getFname());
				pstate.setString(2, user.getLname());
				pstate.setString(3, user.getAddress());
				java.sql.Date sqlDate = new java.sql.Date(user.getDate().getTime());
				pstate.setDate(4, sqlDate);
				pstate.setString(5, user.getUsername());
				pstate.setString(6, user.getPassword());
				
				if(!pstate.execute()) {
					return new User("Data added!");
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return new User("Failed to register user. Please try again.");
		}
		else {
		return new User("Passwords do not match!");
		}
	}
	
}
